(function($){
    $('.notice').click(function (event) {
        var $el = $(event.target);
        if ($el.hasClass('notice-dismiss')) {
            jQuery.post(ajaxurl, {
                'action': 'mtwi_notices',
                'id': $el.parent().attr('id')
            });
        }
    });
})(jQuery);


