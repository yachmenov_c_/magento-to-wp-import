/**
 * @author Yaroslav Yachmenov
 */

var mtwi_imgs_src = {
    loading: mtwi_urls.img + 'loading.gif',
    question: mtwi_urls.img + 'question.png',
    success: mtwi_urls.img + 'success.png',
    exclam: mtwi_urls.img + 'exclam.png'
};

function recalculate_step_and_async(step, async_requests, all_count){
    if (step * async_requests > all_count) {
        do {
            step--;
            if (step * async_requests <= all_count) {
                break;
            }
        } while (step !== 1);
    }
    if (step * async_requests > all_count) {
        do {
            async_requests--;
            if (step * async_requests <= all_count) {
                break;
            }
        } while (async_requests !== 1);
    }
    
    return [step, async_requests];
}

(function ($) {
    if(typeof import_options !== 'object'){
        return;
    }
    var tab = import_options.current_tab, step = parseInt(import_options.step), async_requests = parseInt(import_options.async_requests);
    
    var $progress = $('#mtwi-import > #do-import .progress-block > progress');
    var $do_button = $('#mtwi-import > #do-import button');
    var $action_buttons =  $('#mtwi-import button.button.action').attr('disabled', 'disabled').addClass('button-disabled');
    var $progress_span = $progress.parent().find('span.progress').html(' <img width="20" height="20" src="' + mtwi_imgs_src.loading + '"/>');
    var main_import = function (response) {
        if (typeof response.ids === 'object' && import_options.can_be_launched) {
            var all = response.ids, all_count = all.length, all_backup = all.slice(0), success_all = [];
            [step, async_requests] = recalculate_step_and_async(step, async_requests);
            
            $progress_span.find('img').attr('src', mtwi_imgs_src.success).attr('title', import_options.success_message).tooltip();
            $progress.attr('max', all_count);
            $action_buttons.removeClass('button-disabled').removeAttr('disabled');

            $do_button.click(function () {
                $progress_span.html('').text(': 0 / ' + all_count);
                $action_buttons.attr('disabled', 'disabled').addClass('button-disabled');

                function restore_all(){
                    // Last action after success
                    all = all_backup.slice(0);
                    all_count = all_backup.length;
                    success_all = [];
                    $action_buttons.removeClass('button-disabled').removeAttr('disabled');
                }
                
                function progress_callback() {
                    var success_all_count = success_all.length;
                    if (success_all_count === all_count) {
                        // Success
                        $progress_span
                                .html(' <img width="20" height="20" src="' + mtwi_imgs_src.success + '"/>')
                                .children('img')
                                .attr('title', import_options.done_message).tooltip();
                        setTimeout(restore_all, 100);
                    } else {
                        $progress_span.text(': ' + success_all_count + ' / ' + all_count);
                    }
                    $progress.attr('value', success_all_count);
                }

                function gen_ajax(ids, error_data) {
                    ids = typeof ids === 'undefined' ? all.splice(0, step) : ids;
                    
                    if(ids.length){
                        jQuery.ajax({
                            type: "POST",
                            url: ajaxurl,
                            data: {
                                action: 'mtwi_import',
                                ids: ids,
                                tab: tab,
                                error_data: error_data,
                                last_request: all.length ? 0 : 1
                            },
                            success: function (response) {
                                Array.prototype.push.apply(success_all, ids);
                                progress_callback();
                                if (all.length && response) {
                                    gen_ajax();
                                }
                            },
                            error: function () {
                                for(var j = 0; j < ids.length; j++){
                                    if(typeof error_data === 'undefined'){
                                        gen_ajax([ ids[j] ], {
                                            'count': 1
                                        });
                                    } else {
                                        if(error_data.count < 41){
                                            gen_ajax([ ids[j] ], {
                                                'count': error_data.count + 1
                                            });
                                        }
                                    }
                                }
                            },
                            dataType: 'json'
                        });
                    }
                }

                for (var i = 1; i <= async_requests; i++) {
                    gen_ajax();
                }

            });
        } else {
            var $result_image = $progress_span.find('img');
            $result_image.attr('src', mtwi_imgs_src.exclam);
            if (typeof response.error === 'object' && typeof response.error.errors === 'object' ){
                var error = null, i = 0;
                for(error in response.error.errors){
                    if( i > 1 ) break;
                    i++;
                    $result_image.attr('title', response.error.errors[error][0]).tooltip();
                }
            }
            if($action_buttons.size() === 2){
                $($action_buttons[0]).removeAttr('disabled').removeClass('button-disabled');
            }
        }
    };
    jQuery.post(ajaxurl, {action: 'mtwi_get_ids', tab: tab}, main_import, 'json');
    if(!import_options.is_default_approach && import_options.can_be_launched){
        var $progress_regenerate = $('#mtwi-import > #do-regenerate .progress-block > progress');
        $('#mtwi-import > #do-regenerate button')
        .click(function(){
            $('#mtwi-import button.button.action').attr('disabled', 'disabled').addClass('button-disabled');
            var tmp_action = 'mtwi_generate_tmp_files';
            var $progress_regenerate_span = $progress_regenerate.parent().show().find('span.progress').html(' <img width="20" height="20" src="' + mtwi_imgs_src.loading + '"/>');
            jQuery.post(ajaxurl, {action: tmp_action, tab: tab, first: true}, function (response) {
                if (typeof response.ids === 'object') {
                    var all = response.ids, all_count = all.length, success_all = [];
                    [step, async_requests] = recalculate_step_and_async(step, 1);
                    
                    $progress_regenerate.attr('max', all_count);
                    function restore_all(){
                        $action_buttons.removeClass('button-disabled').removeAttr('disabled');
                        jQuery.post(ajaxurl, {action: 'mtwi_get_ids', tab: tab}, import_options.import_auto_after_dump === 'on' ? function(response){ main_import(response); $do_button.click(); } : main_import, 'json');
                    }
                
                    function progress_callback() {
                        var success_all_count = success_all.length;
                        if (success_all_count === all_count) {
                            // Success
                            $progress_regenerate_span.html(' <img width="20" height="20" src="' + mtwi_imgs_src.success + '"/>');
                            setTimeout(restore_all, 100);
                        } else {
                            $progress_regenerate_span.text(': ' + success_all_count + ' / ' + all_count);
                        }
                        $progress_regenerate.attr('value', success_all_count);
                    }

                    function gen_ajax(ids) {
                        ids = typeof ids === 'undefined' ? all.splice(0, step) : ids;
                        if(ids.length){
                            jQuery.ajax({
                                type: "POST",
                                url: ajaxurl,
                                data: {
                                    action: tmp_action,
                                    ids: ids,
                                    tab: tab
                                },
                                success: function (response) {
                                    Array.prototype.push.apply(success_all, ids);
                                    progress_callback();
                                    if (all.length && response) {
                                        gen_ajax();
                                    }
                                },
                                error: function () {
                                    gen_ajax(ids);
                                },
                                dataType: 'json'
                            });
                        }
                    }

                    gen_ajax();
                    
                } else {
                    $progress_regenerate_span.find('img').attr('src', mtwi_imgs_src.exclam);
                }
            }, 'json');
        });
    }
})(jQuery);
