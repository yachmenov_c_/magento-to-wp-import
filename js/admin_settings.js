/**
 * @author Yaroslav Yachmenov
 * */

/*
 * --- jQuery plugins <---
 * */

jQuery.fn.flag_dependencies = function() {
    var ids_hide = (this.data('dependencies-hide') || '').split(',');
    var ids_show = (this.data('dependencies-show') || '').split(',');
    var ids_hide_selector = '';
    var ids_show_selector = '';
    var $this = this;
    
    for(var i = 0; i < ids_hide.length; i++) ids_hide_selector += '#' + ids_hide[i] + ',';
    if(ids_hide_selector !== '') ids_hide_selector = ids_hide_selector.slice(0, -1);
    
    for(var i = 0; i < ids_show.length; i++) ids_show_selector += '#' + ids_show[i] + ',';
    if(ids_show_selector !== '') ids_show_selector = ids_show_selector.slice(0, -1);
    
    var action = function() {
        if($this.get(0).checked) {
            jQuery(ids_hide_selector).parent().parent().hide(250);
            jQuery(ids_show_selector).parent().parent().show(250);
        } else {
            jQuery(ids_hide_selector).parent().parent().show(250);
            jQuery(ids_show_selector).parent().parent().hide(250);
        }
    };
    
    action();
    this.change(action);
};

jQuery.fn.select_multi_ordering = function(){
    var $this = jQuery(this), $select = $this.clone();
    var $minus = jQuery('<a class="multi-ordering" data-role="minus">&ndash;</a>').insertAfter($this);
    var $plus = jQuery('<a class="multi-ordering" data-role="plus">+</a>').insertAfter($this);
    $select.removeAttr('id').removeAttr('data-others').find('option[selected]').removeAttr('selected');
    
    var others = $this.attr('data-others');
    if(others !== undefined){
        others = JSON.parse(others);
        for(var i = others.length - 1; i >= 0; i--){
            var $new_select = $select.clone();
            $new_select.find('option[value="' + others[i] + '"]').attr('selected', 'selected');
            $new_select.insertAfter($this);
        }
    }
    
    $plus.click(function(){
        $select.clone().insertBefore(jQuery(this));
    });
    
    $minus.click(function(){
        jQuery(this).prev().prev().remove();
    });
};

/*
 * --- jQuery plugins --->
 * */

/*
 * Rules to be appended
 */
jQuery('input[type="checkbox"]').each( function() {jQuery(this).flag_dependencies(); });
jQuery('select[data-type="multi-ordering"][id]:not([multiple])').each( function() {jQuery(this).select_multi_ordering(); });
