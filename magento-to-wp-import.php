<?php

/**
 * Plugin Name: Magento To WP Import
 *
 * Author: Yaroslav Yachmenov
 *
 * Description: Import to your WordPress customers, categories, orders, products.
 *
 * Version: 1.0
 *
 * Author URI: https://www.linkedin.com/in/yaroslav-yachmenov-797945121
 *
 * Text Domain: mtwi
 *
 * Domain Path: languages/
 */
if (!defined('ABSPATH')) {
    die('No script kiddies please!');
}

define('MTWI_URL', plugin_dir_url(__FILE__));
define('MTWI_URL_JS', MTWI_URL . 'js/');
define('MTWI_URL_IMG', MTWI_URL . 'img/');
define('MTWI_URL_CSS', MTWI_URL . 'css/');
define('MTWI_PATH', plugin_dir_path(__FILE__));
define('MTWI_BASENAME', plugin_basename(__FILE__));
define('MTWI_PATH_IMG', MTWI_PATH . 'img/');
define('MTWI_PATH_PHP', MTWI_PATH . 'include/');
define('MTWI_PATH_CLASSES', MTWI_PATH_PHP . 'classes/');
define('MTWI_PATH_TMP', MTWI_PATH . 'tmp/');
define('MTWI_PLUGIN_VERSION', '1.0');

add_action( 'plugins_loaded', 'mtwi_load_plugin_textdomain' );
/**
 * Loaded textdomain for MTWI plugin
 */
function mtwi_load_plugin_textdomain() {
    load_plugin_textdomain( 'mtwi', false, MTWI_BASENAME . '/languages/' );
}

add_action('init', 'mtwi_plugin_init');
/**
 * Entry point for the plugin 
 */
function mtwi_plugin_init() {

    /**
     * Action before MTWI init.
     */
    do_action('mtwi_before_init');
    
    require_once MTWI_PATH_CLASSES . 'MTWI_Page_Single.php';
    require_once MTWI_PATH_CLASSES . 'MTWI_Imports.php';
    require_once MTWI_PATH_CLASSES . 'MTWI_Settings.php';
    require_once MTWI_PATH_PHP . 'functions.php';
    
    /**
     * Action after MTWI init.
     */
    do_action('mtwi_after_init');

}

add_action('mtwi_before_init', 'mtwi_init_tmp_dir', 1);
/**
 * @global WP_Filesystem_Base $wp_filesystem
 */
function mtwi_init_tmp_dir(){
    if( ! function_exists('WP_Filesystem') ){
        require_once ABSPATH . "wp-admin" . '/includes/file.php';
    }
    if(WP_Filesystem()){
        global $wp_filesystem;
        if( ! $wp_filesystem->is_dir(MTWI_PATH_TMP) ){
            $wp_filesystem->mkdir(MTWI_PATH_TMP);
        } elseif( ! $wp_filesystem->is_writable(MTWI_PATH_TMP) ){
            $wp_filesystem->chmod(MTWI_PATH_TMP, 0755);
        }
    }
}

add_action('mtwi_after_init', 'mtwi_after_init_mix');
/**
 *  Mix of different calls before init.
 */
function mtwi_after_init_mix(){
    add_action('wp_ajax_mtwi_notices', 'mtwi_notices');
    if( ! MTWI_Settings::get_instance()->is_start_possible() ){
        add_action( 'admin_notices', 'mtwi_admin_required_options__error');
    }
    if( ! class_exists('SoapClient') ){
        add_action( 'admin_notices', 'mtwi_admin_required_soap__error');
    }
    add_filter('plugin_action_links_' . MTWI_BASENAME, 'mtwi_add_action_links');
}

add_action('admin_enqueue_scripts', 'mtwi_init_admin_media');
function mtwi_init_admin_media($page){
    wp_enqueue_script('mtwi_notices', MTWI_URL_JS . 'notices.js', array('jquery'), MTWI_PLUGIN_VERSION, true);
    wp_localize_script('mtwi_notices', 'mtwi_urls', array('js' => MTWI_URL_JS, 'img' => MTWI_URL_IMG));
    wp_enqueue_style('mtwi_pages', MTWI_URL_CSS . 'admin_pages.css', array(), MTWI_PLUGIN_VERSION, 'all');
}

/**
 * Function for ajax disabling of mtwi notices in the admin panel
 */
function mtwi_notices(){
    $id = filter_input(INPUT_POST, 'id', FILTER_SANITIZE_STRING);
    if($id){
        $notices = get_option('mtwi_notices');
        if(!$notices){
            $notices = array();
        }
        if(strpos($id, '__error') !== false){
            $notices['errors'][$id] = true;
        }
        update_option('mtwi_notices', $notices);
        /* Success */
        wp_die(1);
    }
    
    wp_die(0);
}

/** 
 * * * * * * * * * * * * * * * * * * * *
 * Errors messages for the admin panel *
 * * * * * * * * * * * * * * * * * * * *
 */

function mtwi_admin_required_options__error() {
    mtwi_notice__error(__('Irks! You have not configured required options for <b>Magento</b> import. Please, go to %settings%.', 'mtwi'), __FUNCTION__);
}

function mtwi_admin_required_soap__error(){
    mtwi_notice__error(__('Irks! You have not installed <b>SOAP module</b> for PHP. <b>Magento</b> import plugin requires it for work.', 'mtwi'), __FUNCTION__);
}

/**
 *  Function for outputting formatted error notice, can be used in specified callbacks for wp notice actions
 *
 *  @param string $message Error message
 *  @param string $id Unique id for current notice
 *  @param string $clases Clases separated with gap
 *  @param string $custom_css Custom CSS if needed
 * 
 */
function mtwi_notice__error($message, $id, $clases = 'notice notice-error is-dismissible', $custom_css = ''){
    $notices = get_option('mtwi_notices');
    if(!$notices || ($notices && empty($notices['errors'][$id]))){
        printf('<div class="%s" id="%s" style="%s"><p>%s</p></div>', $clases, $id, $custom_css,
                str_replace('%settings%', '<a href="' . admin_url('admin.php?page=MTWI_Settings') . '">settings</a>', $message));
    }
}

/**
 *  @return array Additional links for the backend plugins menu
 */
function mtwi_add_action_links( $links ) {
    $mylinks = array(
        '<a href="' . admin_url('admin.php?page=MTWI_Settings') . '">' . __('Settings') . '</a>'
    );

    return array_merge($mylinks, $links);
}

register_deactivation_hook( __FILE__, 'mtwi_on_deactivation' );
/**
 * Deactivation hook, used to restore noices, etc.
 */
function mtwi_on_deactivation(){

    if ( ! current_user_can( 'activate_plugins' ) ){
        return;
    }
    
    $plugin = isset( $_REQUEST['plugin'] ) ? $_REQUEST['plugin'] : '';
    check_admin_referer( "deactivate-plugin_$plugin" );

    /* Restore all removed notices by user for using while next activation */
    $notices = get_option('mtwi_notices');
    if ($notices) {
        array_walk($notices, function(&$item){
            foreach($item as &$id){
                $id = false;
            }
        });
    }
    update_option('mtwi_notices', $notices);
    unset($notices);
}

if ( !function_exists('is_ajax') ) {
    function is_ajax() {
        return (defined('DOING_AJAX') && DOING_AJAX);
    }
}
