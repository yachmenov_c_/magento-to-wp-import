<?php

if (!defined('ABSPATH')) {
    die('No script kiddies please!');
}

if(!class_exists('MTWI_Settings')){
    /**
     * MTWI_Settings class for providing of using options in the MTWI plugin
     *
     * @category class
     * 
     * @method array get_options()
     */
    class MTWI_Settings extends MTWI_Page_Single{
        
        static protected $instance = null;

        /** @var array */
        protected $options;

        /** @var array Store in self callbacks objects for futher using them as output functions for <b>texts</b> options */
        public $text_callbacks;

        /** @var array Store in self callbacks objects for futher using them as output functions for <b>checkboxes</b> options */
        public $flag_callbacks;

        /**
         *  @var array Store in self callbacks objects for futher using them as output functions for <b>selects</b> options
         */
        public $select_callbacks;

        /**
         *  @var array Store fields params for using them in the <b>add_fields()</b>
         */
        public $fields_params;

        /**
         *  Constructs ability for using options and constants in the MTWI plugin
         */
        final protected function __construct() {
            parent::__construct();
            $this->page_title = __('Settings', 'mtwi');
            $this->page_tabs = array('general' => __('General Settings', 'mtwi'), 'more_import' => __('More Import Settings', 'mtwi'));
            $this->register_fields_params();
        }

        /**
         *  Method for <b>admin_menu</b> hook. <br>
         *  Start point for all methods in that object.
         */
        function admin_menu() {
            $this->add_sections();
            $this->add_fields();
            add_submenu_page(MTWI_Imports::get_instance()->get_page_name(), $this->page_title, $this->page_title, $this->capability, $this->page_name, array($this, 'page'));
        }

        /**
         *  Registers all params for all fields.<br>
         *  Some of them can be used for init. process of MTWI plugin.<br>
         *  As example: constants <i>MTWI_MAGE_SOAP_URL</i>
         */
        function register_fields_params() {
            $section = $page = 'general';

            $this->register_field_params('mtwi_mage_soap_url', __('Full URL to your SOAP API v1', 'mtwi'), 'text', $page, $section);
            $this->register_field_params('mtwi_mage_soap_user', __('SOAP user', 'mtwi'), 'text', $page, $section);
            $this->register_field_params('mtwi_mage_soap_key', __('SOAP key', 'mtwi'), 'text', $page, $section);
            
            $section = 'import';
            
            $this->register_field_params('mtwi_import_approach', __('Import directly from the server', 'mtwi'), 'flag', $page, $section, array(
                'description' => __('On each import call the plugin will send a request to your Magento. That process really needs a lot of time... But we recommend firstly try this one.', 'mtwi'),
                'dependencies' => array(
                    'hide' => array('mtwi_import_auto_after_dump')
                )
            ));
            
            $this->register_field_params('mtwi_import_auto_after_dump', __('Automatically import after dumping', 'mtwi'), 'flag', $page, $section);
            
            $this->register_field_params('mtwi_import_async_requests', __('How many async requests are allowed?', 'mtwi'), 'text', $page, $section, array(
                'description' => __('Normally plugin uses more than 1 AJAX request in one time to increase the import speed. Do not type a big number here. Even 10 is really fine.', 'mtwi')
            ));
            
            $section = $page = 'more_import';
            
            $this->register_field_params('mtwi_import_customers_step', __('Step of customers import', 'mtwi'), 'text', $page, $section);
            $this->register_field_params('mtwi_import_categories_step', __('Step of categories import', 'mtwi'), 'text', $page, $section);
            $this->register_field_params('mtwi_import_products_step', __('Step of products import', 'mtwi'), 'text', $page, $section);
            $this->register_field_params('mtwi_import_orders_step', __('Step of orders import', 'mtwi'), 'text', $page, $section);
            
            /**
             * @since 1.0
             */
            do_action('mtwi_register_steps', $this, $page, $section);
        }

        /**
         *  Adds all sections for well work of MTWI sub-pages
         */
        function add_sections() {
            $section = $page = 'general';
            add_settings_section(
                    $section, __('Configuration settings', 'mtwi'), function() {
                echo '<p>' . __('Please, configure the options below.', 'mtwi') . '</p>';
            }, $page
            );
            
            $section = 'import';
            add_settings_section(
                    $section, __('Import settings', 'mtwi'), function() {
                echo '<p>' . __('Important options, which can save your time and nerves.', 'mtwi') . '</p>';
            }, $page
            );
            
            $section = $page = 'more_import';
            add_settings_section(
                    $section, __('Other Import settings', 'mtwi'), function() {
                echo '<p>' . __('Here you can specify some more options according to your preferences', 'mtwi') . '</p>';
            }, $page
            );
        }

        /**
         *  Adds fields to related sections and sub-pages. Uses for this <i>$this->fields_params</i> assoc. array.
         */
        function add_fields() {
            foreach ($this->fields_params as $id => $field_params) {
                extract($field_params);
                add_settings_field($id, $title, $callback, $page, $section, $args);
                register_setting($page, $id);
            }
        }
        
        function add_admin_media() {
            remove_action('admin_notices', 'mtwi_admin_required_options__error');
            wp_enqueue_script('mtwi_settings', MTWI_URL_JS . 'admin_settings.js', array('jquery'), MTWI_PLUGIN_VERSION, true);
        }
        
        function is_start_possible(){
            return !(empty($this->options['mage_soap_url']) || empty($this->options['mage_soap_user']) || empty($this->options['mage_soap_key']));
        }
        
        /**
         *  Register settings field params with callback by id to <i>$this->fields_params</i>
         *
         *  @param string $id Id of the option
         *  @param string $title Tittle for the option
         *  @param string $type Type of the option
         *  @param string $page Page for futher displaying
         *  @param string $section Section for futher displaying
         *  @param array $args Arguments to be passed for the option
         * 
         *  @since 1.0 Public access
         *
         */
        function register_field_params($id, $title, $type, $page, $section, $args = array()) {
            $property = "{$type}_callbacks";

            if (property_exists($this, $property)) {
                $method = "create_{$type}_option";

                if (method_exists($this, $method)) {
                    $this->{$property}[$id] = $this->{$method}($id);
                    $this->fields_params[$id] = array(
                        'title' => $title,
                        'callback' => $this->{$property}[$id],
                        'page' => $page,
                        'section' => $section,
                        'args' => $args
                    );
                }
            }
        }

        /**
         *  Creates a suffix from the option id
         *
         *  @param string $id Id of the option
         *  @return string Id part without <i>mtwi_</i> slug
         */
        protected function get_suffix($id) {
            return substr($id, 5);
        }

        /**
         *  Generates html content for the description section under option
         *
         *  @param string $description Description text for an option
         */
        protected function get_description($description) {
            return sprintf('<br><p class="description">%1$s</p>', $description);
        }

        /**
         *  Checks if defined constant can be constant with rule <b>$will_be_constant</b>
         *
         *  @param string $constant_id
         *  @param bool $will_be_constant
         *
         *  @throws LogicException
         *
         */
        protected function can_be_constant_exception($constant_id, $will_be_constant) {
            // TODO: Create specific class for MTWI_Constant_Exception
            if (defined($constant_id) && !$will_be_constant) {
                throw new LogicException("$constant_id can not be defined. It is against the rules!");
            }
        }

        /**
         *  Creates callback specified to checkbox(flag) option
         *
         *  @param string $id Id of option
         *  @return callback
         */
        protected function create_flag_option($id) {
            $suffix = $this->get_suffix($id);
            $option_value = apply_filters('mtwi_flag_value_' . $suffix, get_option($id), $id);

            $this->options[$suffix] = $option_value;

            return function($args) use ($id, $option_value) {
                echo "<input type='checkbox' id='$id' name='$id' " .
                (!empty($option_value) ? 'checked=\'checked\'' : '') .
                (isset($args['dependencies']['hide']) ? 'data-dependencies-hide=\'' . implode(',', $args['dependencies']['hide']) . '\'' : '') .
                (isset($args['dependencies']['show']) ? 'data-dependencies-show=\'' . implode(',', $args['dependencies']['show']) . '\'' : '') .
                " />";

                if (isset($args['description'])) {
                    echo $this->get_description($args['description']);
                }
            };
        }

        /**
         *  Creates callback specified to select option
         *
         *  @param string $id Id of option
         *  @return callback
         *
         */
        protected function create_select_option($id) {
            $suffix = $this->get_suffix($id);
            $option_value = apply_filters('mtwi_select_checked_value_' . $suffix, get_option($id), $id);
            $values_filter = 'mtwi_select_values_' . $suffix;

            $this->options[$suffix] = $option_value;

            return function($args) use ($id, $option_value, $values_filter) {
                $is_multi_ordering = empty($args['multi_ordering']) ? false  : true;
                $main_value = ($is_multi_ordering && is_array($option_value))  ? current($option_value) : $option_value;
                $values = apply_filters($values_filter, array($main_value => $main_value));
                printf('<select id="%s" name="%s" %s %s>', $id, $is_multi_ordering ? $id . '[]' : $id, $is_multi_ordering ? 'data-type="multi-ordering"' : '',
                        ($is_multi_ordering && count($option_value) > 1) ? 'data-others="' . esc_attr(wp_json_encode(array_slice($option_value, 1))) . '"' : '');
                foreach ($values as $s_id => $item_value) {
                    printf('<option value="%s" %s>%s</option>', $s_id, $s_id == $main_value ? 'selected' : '', $item_value);
                }
                print('</select>');

                if (isset($args['description'])) {
                    echo $this->get_description($args['description']);
                }
            };
        }

        /**
         *  Creates callback specified to text option. <br>
         *  Also can <b>define</b> the constant from the option or vice versa, if needed.
         *
         *  @param string $id Id of option
         *  @return callback
         *
         */
        protected function create_text_option($id) {
            $suffix = $this->get_suffix($id);
            $constant_id = strtoupper($id);
            $will_be_constant = apply_filters('mtwi_text_will_be_constant_' . $suffix, false, $id, $constant_id);

            $this->can_be_constant_exception($constant_id, $will_be_constant);

            if (!($is_constant = defined($constant_id))) {
                $option_value = apply_filters('mtwi_text_value_' . $suffix, get_option($id), $id);
                if ($will_be_constant) {
                    define($constant_id, $option_value);
                }
            } else {
                $option_value = constant($constant_id);
            }
            
            $this->options[$suffix] = $option_value;

            return function($args) use ($id, $option_value, $is_constant) {
                $format = '<input type="text" class="regular-text code %1$s" %1$s id="%2$s" name="%2$s" value="%3$s">';
                printf($format, $is_constant ? 'disabled' : '', $id, esc_attr($option_value));

                if (isset($args['description'])) {
                    echo $this->get_description($args['description']);
                }
            };
        }

        /**
         *  Registers filters, used in the settigns page, mainly for options
         */
        protected function register_filters() {
            add_filter('mtwi_text_will_be_constant_mage_soap_url', array($this, 'text_constants_filter'), 10, 2);
            add_filter('mtwi_text_will_be_constant_mage_soap_user', array($this, 'text_constants_filter'), 10, 2);
            add_filter('mtwi_text_will_be_constant_mage_soap_path', array($this, 'text_constants_filter'), 10, 2);
            add_filter('mtwi_flag_value_import_approach', function($value){
                return $value === false ? 'on' : $value;
            });
            
            $default_step = function($value){
                return $value === false ? 10 : intval($value);
            };
            
            add_filter('mtwi_text_value_import_async_requests', $default_step);
            add_filter('mtwi_text_value_import_customers_step', $default_step);
            add_filter('mtwi_text_value_import_categories_step', $default_step);
            add_filter('mtwi_text_value_import_products_step', $default_step);
            add_filter('mtwi_text_value_import_orders_step', $default_step);

            do_action('mtwi_register_settings_filters');
        }

        /* FILTERS */

        function text_constants_filter($will_be_constant, $id) {
            switch ($this->get_suffix($id)) {
                case 'mage_soap_url':
                case 'mage_soap_user':
                case 'mage_soap_key':
                    $will_be_constant = true;
            }

            return $will_be_constant;
        }

        function page_output($atts = array()) {
            $active_tab = $this->get_active_tab();
            ?>
            <div class="wrap">
                <div class="options-panel">
                    <h2 class="logo" style="background-image: url('<?php esc_attr_e($this->logo_url); ?>');"><?php echo MTWI_Imports::get_instance()->get_page_title() . ' - ' . $this->page_title; ?></h2>

                    <?php $this->page_tabs_output(); ?>

                    <form method="post" action="options.php">
                        <?php
                        settings_fields($active_tab);

                        do_settings_sections($active_tab);

                        submit_button();
                        ?>
                    </form>
                </div>
            </div>
            <?php
        }

    }
}

MTWI_Settings::init();

$GLOBALS['mtwi_options'] = MTWI_Settings::get_instance()->get_options();