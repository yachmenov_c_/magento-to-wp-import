<?php
if (!defined('ABSPATH')) {
    die('No script kiddies please!');
}

if (!class_exists('MTWI_Imports')) {

    /**
     * MTWI_Imports
     * @category class
     */
    class MTWI_Imports extends MTWI_Page_Single {

        static protected $instance = null; 
        public $logo_svg64;

        final protected function __construct() {
            parent::__construct();
            $this->page_title = __('Magento to WP Import', 'mtwi');
            $this->logo_svg64 = 'data:image/svg+xml;base64,' . base64_encode(file_get_contents(MTWI_PATH_IMG . 'logo.svg'));
            $this->page_tabs = apply_filters('mtwi_import_page_tabs', array(
                'customers' => __('Customers', 'mtwi'),
                'categories' => __('Categories', 'mtwi'),
                'products' => __('Products', 'mtwi'),
                'orders' => __('Orders', 'mtwi')
            ));
        }

        function add_admin_media() {
            global $mtwi_options;
            $active_tab = $this->get_active_tab();
            
            wp_enqueue_script('mtwi_imports', MTWI_URL_JS . 'admin_imports.js', array('jquery', 'jquery-ui-tooltip'), MTWI_PLUGIN_VERSION, true);
            wp_localize_script('mtwi_imports', 'import_options', array(
                'current_tab' => $active_tab,
                'is_default_approach' => mtwi_is_default_approach(),
                'can_be_launched' => MTWI_Settings::get_instance()->is_start_possible(),
                'step' => $mtwi_options["import_{$active_tab}_step"],
                'async_requests' => $mtwi_options['import_async_requests'],
                'import_auto_after_dump' => $mtwi_options['import_auto_after_dump'],
                'success_message' => __('Import can be launched', 'mtwi'),
                'done_message' => __('Import has been successfully done!', 'mtwi')
            ));
        }

        function admin_menu() {
            add_menu_page($this->page_title, $this->page_title, $this->capability, $this->page_name, array($this, 'page'), $this->logo_svg64);
            add_submenu_page($this->page_name, $this->page_title, __('Import', 'mtwi'), $this->capability, $this->page_name);
        }

        function page_output($atts = array()) {
            $active_tab = $this->get_active_tab();
            ?>
            <div class="wrap">
                <div class="options-panel">
                    <h2 class="logo" style="background-image: url('<?php esc_attr_e($this->logo_url); ?>');"><?php echo $this->page_title; ?></h2>
            <?php $this->page_tabs_output(); ?>
                    <div id="mtwi-import">
                    <?php
                    if (array_key_exists($active_tab, $this->page_tabs)) {
                        $this->process_tab($active_tab);
                    } else {
                        $this->process_tab('customers');
                    }
                    ?>
                    </div>    
                </div>
            </div>
            <?php
        }

        function ajax_import() {
            $this->ajax_hard(function() {
                $ids = filter_input(INPUT_POST, 'ids', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
                $tab = filter_input(INPUT_POST, 'tab', FILTER_SANITIZE_STRING);
                $GLOBALS['mtwi_ajax_error_data'] = filter_input(INPUT_POST, 'error_data', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
                $GLOBALS['mtwi_is_last_request'] = filter_input(INPUT_POST, 'last_request', FILTER_VALIDATE_BOOLEAN, FILTER_REQUIRE_SCALAR);

                if ($ids && $tab)
                    if(isset($this->page_tabs[$tab]))
                        wp_die(call_user_func("mtwi_import_{$tab}", call_user_func("mtwi_get_{$tab}_list", $ids)));
            });
        }

        function ajax_get_ids() {
            $this->ajax_hard(function() {
                $tab = filter_input(INPUT_POST, 'tab', FILTER_SANITIZE_STRING);
                $list_func_name = "mtwi_get_{$tab}_list";
                $out_ids = function () use($list_func_name, $tab) {
                    list(, $id_column) = mtwi_get_group_props($tab);
                    
                    if(function_exists($list_func_name))
                        $results = call_user_func($list_func_name);
                    
                    if (is_array($results) && $results) {
                        $final = array(
                            'ids' => wp_list_pluck($results, $id_column)
                        );
                        wp_die( wp_json_encode($final) );
                    } elseif( is_wp_error($results) ) {
                        wp_die( wp_json_encode(array('error' => $results)) );
                    } else {
                        wp_die(0);
                    }
                };
                
                if(isset($this->page_tabs[$tab]))
                    $out_ids();
            });
        }
        
        function ajax_generate_tmp_files(){
            $this->ajax_hard(function() {
                $tab = filter_input(INPUT_POST, 'tab', FILTER_SANITIZE_STRING);
                
                if(!$tab){
                    wp_die(0);
                }
                
                $ids = filter_input(INPUT_POST, 'ids', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
                $first = filter_input(INPUT_POST, 'first', FILTER_VALIDATE_BOOLEAN);
                
                list($names, $id_key) = mtwi_get_group_props($tab);
                if(!$ids && $first){
                    $list_name = array_shift($names);
                    $ids = _mtwi_get_soap_data($list_name);
                }
                
                if ($ids && $id_key) {
                    if(isset($this->page_tabs[$tab])){
                        if($first){
                            if(_mtwi_put_tmp_file(mtwi_get_tmp_full_filename($list_name), $ids)){
                                wp_die(wp_json_encode(array('ids' => wp_list_pluck($ids, $id_key))));
                            }
                        } else {
                            wp_die(mtwi_process_tmp_files($tab, $ids, $first));
                        }
                    }
                }
            });
        }

        protected function register_actions() {
            parent::register_actions();
            add_action('wp_ajax_mtwi_import', array($this, 'ajax_import'));
            add_action('wp_ajax_mtwi_get_ids', array($this, 'ajax_get_ids'));
            add_action('wp_ajax_mtwi_generate_tmp_files', array($this, 'ajax_generate_tmp_files'));
        }

        protected function process_tab($tab) {
            $title = $this->page_tabs[$tab];
            $default_approach = mtwi_is_default_approach();
            ?>
            <?php if (!$default_approach): ?>
                <div id="do-regenerate">
                    <button class="button action"><?php _e('Update/create local copies of data to be imported', 'mtwi'); ?></button>
                    <div class="progress-block" hidden="hidden">
                        <label><?php echo __('Progress of', 'mtwi'), ' ', esc_html($title), ' ', __('local files', 'mtwi') ?><span class="progress"></span></label>
                        <progress value="0"></progress> 
                    </div>
                </div>
            <?php endif; ?>
            <div id="do-import">
                <div class="progress-block<?php echo ($default_approach ? ' single' : ' '); ?>">
                    <label><?php echo __('Progress of', 'mtwi'), ' ', esc_html($title); ?><span class="progress"></span></label>
                    <progress value="0"></progress>
                </div>
                <button class="button button-primary button-large button-hero action"><?php _e('Do import!', 'mtwi'); ?></button>
            </div>
            <?php
        }

    }

}

MTWI_Imports::init();
