<?php

if (!defined('ABSPATH')) {
    die('No script kiddies please!');
}

/**
 * MTWI_Page_Single
 * 
 * @category class
 * 
 * @method string get_page_name()
 * @method string get_page_title()
 * @method string get_active_tab()
 * @method array  get_page_tabs()
 */
abstract class MTWI_Page_Single {
    
    static protected $instance = null;

    protected $page_name;
    
    protected $page_tabs;
    
    protected $logo_url;
    
    protected $page_title = '';

    protected $capability = 'manage_options';

    /**
     * Init. once a time
     */
    static final function init(){
        if(static::$instance === null){
            static::$instance = new static;
        }
    }

    /**
     * Return instance of current class
     *
     * @return static
     */
    static final function get_instance(){
        return static::$instance;
    }

    protected function __construct() {
        $this->page_name = get_class($this);
        $this->logo_url = MTWI_URL_IMG . 'logo.svg';
        $this->register_actions();
        $this->register_filters();
    }
    
    final function init_admin_media($page){
        if (strpos($page, $this->page_name) === false) {
            return;
        }
        $this->add_admin_media();
    }
    
    final function page($atts = array()){
        if ( !current_user_can($this->capability) )  {
            wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
        }
        $this->page_output($atts);
    }
    
    final function __call($name, $params) {
        if(strpos($name, 'get_') === 0){
            $name = substr($name, 4);
            
            switch($name){
                case 'active_tab':
                    $tab = filter_input(INPUT_GET, 'tab', FILTER_SANITIZE_STRING);
                    return $tab ? $tab : key($this->page_tabs);
                default:
                    if(property_exists($this, $name)){
                        return $this->{$name};
                    }
                    break;
            }
            
            if(method_exists($this, $method_name = '__call_aditional')){
                return call_user_func_array(array($this, $method_name), array($name, $params));
            }
        }

        return null;
    }
    
    final function page_tabs_output(){
        $active_tab = $this->get_active_tab();
        echo '<h2 class="nav-tab-wrapper">';
        foreach($this->page_tabs as $key => $name){
            printf('<a href="?page=%s&tab=%s" class="nav-tab%s">%s</a>', $this->page_name, $key, $active_tab === $key ? ' nav-tab-active' : '', $name);
        }
        echo '</h2>';
    }
    
    protected function ajax_hard(callable $what_to_do){
        if ( !current_user_can($this->capability) )  {
            return;
        }
        ignore_user_abort(1);
        set_time_limit(0);
        call_user_func($what_to_do);
        wp_die(0);
    }
    
    /**
     *  Registers actions
     */
    protected function register_actions(){
        add_action('admin_enqueue_scripts', array($this, 'init_admin_media'));
        add_action('admin_menu', array($this, 'admin_menu'));
    }
    
    /**
     *  Registers filters
     */
    protected function register_filters(){}

    /**
     * FUNCTION OF RENDERING PAGE
     */
    abstract function page_output($atts = array());
    
    /**
     * Add media only in this page
     */
    abstract function add_admin_media();
    abstract function admin_menu();
            
}