<?php

/**
 * Making/checking connect via SOAP API v1 to Magento 1
 * 
 * @return array|WP_Error
 */
function mtwi_soap_connect(){
    static $result = null;
    
    if( is_null($result) ){
        
        if( class_exists('SoapClient') ) {
        
            global $mtwi_options;

            $mage_url = $mtwi_options['mage_soap_url'];
            $api_user = $mtwi_options['mage_soap_user'];
            $api_key = $mtwi_options['mage_soap_key'];

            try {
                $client = new SoapClient($mage_url);
                $session = $client->login($api_user, $api_key);
                $result = empty($session) ? 
                          new WP_Error('soap_error', __('Soap login failed, please check your crends', 'mtwi')) :
                          array($client, $session);
            } catch (SoapFault $soap_fault) {
                $wp_error = new WP_Error('soap_error', __('Can not connect/login to SOAP API: ', 'mtwi') . $soap_fault->getMessage());
                $result = $wp_error;
            }
        
        } else {
            $result = new WP_Error('soap_error', __('SoapClient is not configured on your PHP installation', 'mtwi'));
        }
        
    }
    
    return $result;
}

function mtwi_is_default_approach(){
    global $mtwi_options;
    return $mtwi_options['import_approach'] === 'on' ? true : false;
}

function mtwi_get_tmp_full_filename($filename){
    return MTWI_PATH_TMP . $filename . '.json';
}

function mtwi_get_group_props($group_name){
    switch($group_name){
        case 'customers':
            $names = array('customer.list', 'customer_address.list');
            $id_key = 'customer_id';
            break;
        case 'categories':
            $names = array('catalog_category.tree', 'catalog_category.info');
            $id_key = 'category_id';
            break;
        case 'products':
            $names = array(
                'catalog_product.list',
                'catalog_product.info',
                'cataloginventory_stock_item.list',
                'catalog_product_attribute_media.list',
                'product_custom_option.list',
                'catalog_product_link.list'
            );      
            $id_key = 'product_id';
            break;
        case 'orders':
            $names = array(
                'sales_order.list',
                'sales_order.info'
            );
            $id_key = 'increment_id';
            break;
        default:
            /**
             * @since 1.0
             */
            $names = apply_filters('mtwi_get_group_names', array(), $group_name);
            /**
             * @since 1.0
             */
            $id_key = apply_filters('mtwi_get_group_id_key', '', $group_name);
            break;
    }
    
    return array($names, $id_key);
}

/**
 * @param string $group_name Name of group to know which json files should it create/update (example <b>customers | categories | products</b>)
 * @param array $ids 
 * @param bool $is_first_start Is it first start? (<i>for ajax usage</i>)
 * @global WP_Filesystem_Base $wp_filesystem
 * 
 * @return null | bool <b>null</b> when nothing to do
 */
function mtwi_process_tmp_files($group_name, array $ids = array(), $is_first_start = false){
    global $wp_filesystem;
    if( ! is_object($wp_filesystem) ){
        return false;
    }
    
    $read_files = $data = array();
    list($names, $id_key) = mtwi_get_group_props($group_name);
    if(count($names) === 1)
        return true;
    
    if( ! $names ){
        return false;
    }
    foreach($names as $key => $filename){
        $filepath = mtwi_get_tmp_full_filename($filename);
        if( $wp_filesystem->exists( $filepath ) ){
            if( $is_first_start ){
                $wp_filesystem->delete($filepath, false, 'f');
            } elseif(!$ids) {
                unset($names[$key]);
            } else {
                $read_files[] = $filename;
            }
        }
    }
    if( ! $names ){
        return null;
    }
    if( ! _mtwi_set_list_and_related($data, $names, $id_key, $ids, $is_first_start) ){
        return false;
    }
    
    if( $ids && ! $is_first_start ){
        array_shift($names);
    }
    foreach($data as $filename => $content){
        $filepath = mtwi_get_tmp_full_filename($filename);
        settype($content, 'array');
        /** @TODO Remove pretty print */
        if(in_array($filename, $read_files)){
            $content = (array)json_decode($wp_filesystem->get_contents($filepath), true) + $content;
        }
        _mtwi_put_tmp_file($filepath, $content);
    }
    
    return true;
}

/**
 * @access private
 * 
 * @param string $filepath
 * @param array $content Content to be saved 
 * @param int $json_options Json oprions like JSON_PRETTY_PRINT
 * @param int $mode File mode
 * @global WP_Filesystem_Base $wp_filesystem
 * 
 * @return bool False on failure
 */
function _mtwi_put_tmp_file($filepath, array $content = array(), $json_options = 0, $mode = 0644){
    global $wp_filesystem;
    return $wp_filesystem->put_contents($filepath, wp_json_encode($content, $json_options), intval($mode, 8));
}

/**
 * @access private
 */
function _mtwi_set_list_and_related(array &$data, array $names, $key_id, array $ids = array(), $is_first_start = false){
    $list_name = array_shift($names);
    if( ! $ids || $is_first_start){
        $data[$list_name] = _mtwi_get_soap_data($list_name);
        $to_process = $data[$list_name];
    }
    if( $ids ){
        $to_process = array();
        foreach($ids as $id){
            $to_process[] = array($key_id => $id);
        }
    }
    foreach($names as $name){
        foreach($to_process as $l){
            $data[$name][$l[$key_id]] = _mtwi_get_soap_data($name, $l[$key_id]);
        }
    }
    
    return $data;
}

/**
 * @access private
 * 
 * @return array|WP_Error
 */
function _mtwi_get_soap_data(){
    $pre_args = func_get_args();
    /**
     * @since 1.0
     */
    $args = apply_filters("mtwi_soap_args_$pre_args[0]", $pre_args);
    $connect = mtwi_soap_connect();
    $result = array();

    if( is_array($connect) ){
        list($client, $session) = $connect;
        try{
            $result = apply_filters("mtwi_soap_data_$args[0]",
                    call_user_func_array(array($client, 'call'), array_merge((array)$session, $args)),
                    $client, $session, $args);
        } catch (SoapFault $soap_fault){
            $result = false;
        } finally {
            /**
             * @since 1.0
             */
            $result = apply_filters("mtwi_soap_data_$args[0]_finally", $result, $client, $session, $args);
        }
    } else {
        $result = $connect;
    }
    
    return $result;
}

add_filter('mtwi_soap_data_catalog_category.tree', 'mtwi_categories_tree_to_array_filter', 10, 3);
function mtwi_categories_tree_to_array_filter($cats_tree, SoapClient $client, $session){
    $final_result = array();

    foreach($cats_tree['children'] as $cat){
        $category_details = $client->call($session, 'catalog_category.info', $cat['category_id']);
        $all_children = explode(',', $category_details['all_children']);
        $children_tree = $cat['children'];

        unset($cat['children']);
        $final_result[] = $cat;

        foreach($all_children as $child_id){
            $subcat = mtwi_recursive_search($children_tree, 'category_id', $child_id);
            if($subcat){
                unset($subcat['children']);
                $final_result[] = $subcat;
            }
        }
    }

    return $final_result;
}

/**
 * @access private
 */
function _mtwi_set_data(&$data, $soap_name){
    
    if(is_null($data)){
        if(mtwi_is_default_approach()){
            // Only for lists works
            $data = _mtwi_get_soap_data($soap_name);
        } else {
            $filename = mtwi_get_tmp_full_filename($soap_name);
            if(file_exists($filename)){
                $data = json_decode(file_get_contents($filename), true);
            }
        }
    }
    
    return $data;
}

/**
 * @access private
 */
function _mtwi_set_list(&$list, $soap_name, $id_key = '', array $ids = array()){
    _mtwi_set_data($list, $soap_name);
    
    if(!empty($ids) && is_array($list)){
        return mtwi_filter_list($list, $id_key, $ids);
    }
    
    return $list;
}

function mtwi_filter_list(array $list, $id_key, array $ids){
    foreach($list as $key => $res){
        if(!in_array($res[$id_key], $ids)){
            unset($list[$key]);
        }
    }
    return array_values($list);
}

function mtwi_get_customers_list(array $ids = array()){
    static $list = null;
    list($names, $id_key) = mtwi_get_group_props('customers');
    return _mtwi_set_list($list, $names[0], $id_key, $ids);
}

function mtwi_get_products_list(array $ids = array()){
    static $list = null;
    list($names, $id_key) = mtwi_get_group_props('products');
    return _mtwi_set_list($list, $names[0], $id_key, $ids);
}

function mtwi_get_categories_list(array $ids = array()){
    static $list = null;
    list($names, $id_key) = mtwi_get_group_props('categories');
    return _mtwi_set_list($list, $names[0], $id_key, $ids);
}

function mtwi_get_orders_list(array $ids = array()){
    static $list = null;
    list($names, $id_key) = mtwi_get_group_props('orders');
    return _mtwi_set_list($list, $names[0], $id_key, $ids);
}

/**
 *  @access private 
 * 
 *  return mixed|WP_Error
 */
function _mtwi_mix_info(&$data, $name, $id){
    if(mtwi_is_default_approach()){
        return _mtwi_get_soap_data($name, $id);
    } else {
        _mtwi_set_data($data, $name);
        return isset($data[$id]) ? $data[$id] : false;
    }
}

function mtwi_get_customer_address_info($customer_id){
    static $addresses = null, $name = 'customer_address.list';
    return _mtwi_mix_info($addresses, $name, $customer_id);
}

function mtwi_import_customers(array $customers){
        
    // Parse the reterived customer details
    foreach ($customers as $customer_data) {

        $customer_id = $customer_data['customer_id'];
        $customer_email = $customer_data['email'];

        // Insert customer informations into WordPress
        $wp_customer_id = mtwi_insert_customer($customer_data);

        // Reterive customer address info
        $customer_address_details = mtwi_get_customer_address_info($customer_id);
        if ($customer_address_details) {
            foreach ($customer_address_details as $addr_data) {
                if ($addr_data['is_default_billing']) {
                    $billing_details = array(
                        'billing_first_name' => $addr_data['firstname'],
                        'billing_last_name' => $addr_data['lastname'],
                        'billing_company' => isset($addr_data['company']) ? $addr_data['company'] : '',
                        'billing_address_1' => $addr_data['street'],
                        'billing_city' => $addr_data['city'],
                        'billing_postcode' => $addr_data['postcode'],
                        'billing_country' => $addr_data['country_id'],
                        'billing_state' => $addr_data['region'],
                        'billing_phone' => $addr_data['telephone'],
                        'billing_email' => $customer_email
                    );
                }
                if ($addr_data['is_default_shipping']) {
                    $shipping_details = array(
                        'shipping_first_name' => $addr_data['firstname'],
                        'shipping_last_name' => $addr_data['lastname'],
                        'shipping_company' => isset($addr_data['company']) ? $addr_data['company'] : '',
                        'shipping_address_1' => $addr_data['street'],
                        'shipping_city' => $addr_data['city'],
                        'shipping_postcode' => $addr_data['postcode'],
                        'shipping_country' => $addr_data['country_id'],
                        'shipping_state' => $addr_data['region']
                    );
                }
                if (!empty($shipping_details)) {
                    mtwi_set_customer_address($wp_customer_id, $shipping_details);
                }
                if (!empty($billing_details)) {
                    mtwi_set_customer_address($wp_customer_id, $billing_details);
                }
            }
        }
    }
    
    return true;
}

function mtwi_insert_customer($customer_info){
    
    $customer_email = $customer_info['email'];
    $user_login = explode('@', $customer_email)[0];
    $customer_firstname = $customer_info['firstname'];
    $customer_lastname = $customer_info['lastname'];

    $user_details = array(
        'user_login' => $user_login,
        'user_pass' => $customer_info['password_hash'],
        'user_nicename' => $user_login,
        'user_email' => $customer_email,
        'user_registered' => $customer_info['created_at'],
        'first_name' => $customer_firstname,
        'last_name' => $customer_lastname,
        'display_name' => "$customer_firstname $customer_lastname",
        'role' =>  'customer'
    );

    // User creation
    $user_id = wp_insert_user($user_details);

    return $user_id;
}

function mtwi_set_customer_address($customer_id, $customer_address) {
    foreach ($customer_address as $key => $addr_value) {
        update_user_meta($customer_id, $key, $addr_value);
    }
}

/**
 * @param array $categories
 * @global bool $mtwi_is_last_request
 */
function mtwi_import_categories(array $categories) {
    global $mtwi_is_last_request;
    $taxonomy = 'product_cat';
    
    foreach ($categories as $categ_data) {
        $categ_id = $categ_data['category_id'];
        $category_details = mtwi_get_category_info($categ_id);
        
        $wp_term = term_exists($category_details['url_key'], $taxonomy);
        $image = $category_details['thumbnail'] ? $category_details['thumbnail'] : $category_details['image'];
        
        if( ! $wp_term || $category_details['parent_id'] && ! mtwi_is_equal_to_magento_term($wp_term, $category_details['parent_id']) ){
            mtwi_insert_category($category_details['parent_id'], $category_details['name'], $category_details['url_key'], $category_details['description'], $image);
        } elseif( ! ( $attachment_id = get_woocommerce_term_meta( $wp_term['term_id'], 'thumbnail_id') ) &&  $image ) {
            $attachment_id = mtwi_insert_product_cat_attachment($image, $wp_term['term_id'], $category_details['name']);
        }
        
        if( ! empty($attachment_id) ){
            // For dublicates also asign $attachment_id, sometimes can be useful
            $terms = get_terms(array(
                'taxonomy' => $taxonomy,
                'name' => $category_details['name'],
                'hide_empty' => 0,
                'exclude' => array( $wp_term['term_id'] )
            ));
            if( $terms && ! is_wp_error($terms) )
                /* @var $term WP_Term */
                foreach($terms as $term)
                    update_woocommerce_term_meta( $term->term_id, 'thumbnail_id', $attachment_id );
        }
    }
    
    if($mtwi_is_last_request){
        mtwi_delete_terms_dublicates();
    }

    return true;
}

/**
 * @param array $wp_term
 * @param int|string $magento_parent_id 
 * @return bool
 */
function mtwi_is_equal_to_magento_term($wp_term, $magento_parent_id){
    $taxonomy = 'product_cat';
    
    if(is_array($wp_term)){
        $parent_info = mtwi_get_category_info($magento_parent_id);
        $wp_parent = get_term_by('slug', $parent_info['url_key'], $taxonomy);
        $wp_child = get_term_by('id', $wp_term['term_id'], $taxonomy);
        return $wp_child->parent === $wp_parent->term_id;
    }
    
    return false;
}

function mtwi_delete_terms_dublicates($taxonomy = 'product_cat'){
    $terms = get_terms(array(
        'taxonomy' => $taxonomy,
        'hide_empty' => 0
    ));
    $for_deletion = array();
    for($i = 0; $i < count($terms); $i++)
        for($j = 0; $j < count($terms); $j++)
            if($i !== $j && ! in_array($j, $for_deletion) && $terms[$i]->name === $terms[$j]->name && $terms[$i]->parent === $terms[$j]->parent)
                $for_deletion[] = $terms[$i]->count < $terms[$j]->count ? $i : $j;
    foreach($for_deletion as $key) 
        wp_delete_term($terms[$key]->term_id, $taxonomy);
}

function mtwi_get_category_info($cat_id) {
    static $cats_info = null, $name = 'catalog_category.info';
    return _mtwi_mix_info($cats_info, $name, $cat_id);
}


function mtwi_insert_category($parent_id, $categ_name, $categ_slug, $categ_desc, $categ_image) {
    $taxonomy = 'product_cat';

    if ($parent_id == 1) {
        $parent_id = 0;
    } else {
        $parent_categ_info = mtwi_get_category_info($parent_id);
        $wp_parent_cat_name = $parent_categ_info['name'];
        $wp_parent_cat_details = get_term_by('name', $wp_parent_cat_name, $taxonomy);
        if ($wp_parent_cat_details) {
            $parent_id = $wp_parent_cat_details->term_id;
        } else {
            $parent_id = 0;
        }
    }
    
    $inserted_term_id = wp_insert_term($categ_name, $taxonomy, array('description' => $categ_desc, 'slug' => $categ_slug, 'parent' => $parent_id));
    if (!is_wp_error($inserted_term_id)) {
        $term_id = $inserted_term_id['term_id'];
        if (!empty($categ_image)) {
            mtwi_insert_product_cat_attachment($categ_image, $term_id, $categ_name);
        }
    } else {
        $term_id = false;
    }
}

function mtwi_import_products(array $products) {
    $cat_taxonomy = 'product_cat';

    foreach($products as $product_data) {
        $product_id = $product_data['product_id'];
        $product_details = mtwi_get_product_info($product_id);
        $sku = $product_details['sku'];
        $wp_product_id = 0;
        $current_user_id = get_current_user_id();
        $created = false;

        // Single Product Base Information
        $single_product = array(
            'post_title' => $product_details['name'],
            'post_content' => $product_details['description'],
            'post_excerpt' => $product_details['short_description'] ? $product_details['description'] : '',
            'post_status' => $product_details['status'] == 1 ? 'publish' : 'draft',
            'post_date' => date('Y-m-d H:i:s', strtotime($product_details['created_at'])),
            'post_modified' => date('Y-m-d H:i:s', strtotime($product_details['updated_at'])),
            'post_author' => $current_user_id ? $current_user_id : 1,
            'post_name' => $product_details['url_key'],
            'comment_status' => 'open',
            'ping_status' => 'open',
            'post_type' => 'product'
        );

        if (function_exists('wc_get_product_id_by_sku')) {
            $wp_product_id = wc_get_product_id_by_sku($sku);
            if($wp_product_id){
                $reformed_product = array('ID' => $wp_product_id) + $single_product;
                unset($reformed_product['post_date']);
                if($reformed_product['post_status'] === 'draft'){
                  unset($reformed_product['post_modified']);  
                }
                wp_update_post($reformed_product);
            }
        }
        if (!$wp_product_id) {
            $wp_product_id = wp_insert_post($single_product);
            $new_product_meta['_sku'] = $sku;
            $created = true;
        }
        if (!$wp_product_id) {
            continue;
        }

        if( ! $created && function_exists('wc_get_product_cat_ids') )
            wp_remove_object_terms($wp_product_id, wc_get_product_cat_ids($wp_product_id), $cat_taxonomy);
        mtwi_add_cats_to_product($wp_product_id, $product_details['categories']);

        $new_product_meta['_visibility'] = mtwi_get_visibility($product_details['visibility']);

        _mtwi_process_product_media($new_product_meta, $product_id, $wp_product_id, $single_product['post_title'], $created);
        _mtwi_process_product_stock($new_product_meta, $product_id);
        _mtwi_process_product_price($new_product_meta, $product_details['price'], $product_details['special_price']);

        $new_product_meta['_sale_price_dates_from'] = strtotime($product_details['special_from_date']);
        $new_product_meta['_sale_price_dates_to'] = strtotime($product_details['special_to_date']);

        $new_product_meta['_tax_status'] = mtwi_get_tax_status($product_details['tax_class_id']);
        $new_product_meta['_weight'] = $product_details['weight'];
        $new_product_meta['_product_url'] = $product_details['url_path'];
        $new_product_meta['_length'] = $new_product_meta['_width'] = $new_product_meta['_height'] = 0;
        $new_product_meta['_tax_class'] = $new_product_meta['_purchase_note'] = $new_product_meta['_featured'] = $new_product_meta['_backorders'] = '';
        
        $product_type = 'simple';
        $custom_options = mtwi_get_product_custom_options_info($product_id);
        if($custom_options){
            $product_type = 'variable';
            _mtwi_process_product_variations($new_product_meta, $wp_product_id, $custom_options, $created);
        }
        
        $linked_info = mtwi_get_product_linked_info($product_id);
        foreach($linked_info as $type => $linked_products){
            if($linked_products){
                $meta_key = '_' . str_replace('_', '', $type) . '_ids';
                $wp_linked_products = array();
                foreach($linked_products as $linked_product){
                    $wp_linked_id = wc_get_product_id_by_sku($linked_product['sku']);
                    if($wp_linked_id)
                        $wp_linked_products[] = $wp_linked_id;
                }
                $new_product_meta[$meta_key] = $wp_linked_products;
            }
        }

        wp_set_object_terms($wp_product_id, $product_type, 'product_type');
        
        foreach ($new_product_meta as $meta_key => $meta_value) {
            update_post_meta($wp_product_id, $meta_key, $meta_value);
        }
        
        wc_delete_product_transients($wp_product_id);
    }

    return true;
}

function mtwi_get_visibility($mage_visibility_id){
    switch($mage_visibility_id){
        case 1:
            return 'hidden';
        case 2:
            return 'catalog';
        case 3:
            return 'search';
        default:
            return 'visible';
    }
}

function mtwi_get_tax_status($mage_tax_class_id){
    switch($mage_tax_class_id){
        case 2:
            return 'taxable';
        case 3:
            return 'shipping';
        default:
            return 'none';
    }
}

/**
 * @param array $new_meta
 * @param int $wp_product_id
 * 
 */
function _mtwi_process_product_variations(array &$new_meta, $wp_product_id, array $mage_custom_options, $created) {
    if (!$created) {
        $variations = get_posts(array('post_type' => 'product_variation', 'post_parent' => $wp_product_id, 'posts_per_page' => -1));
        if ($variations) {
            /* @var $variation WP_Post */
            foreach ($variations as $variation) {
                wp_delete_post($variation->ID, true);
            }
        }
    }

    $all = array();
    foreach ($mage_custom_options as $key => &$option) {
        $available_values = array();
        $sanitized_title = sanitize_title($option['title']);
        $attr_taxonomy_name = wc_attribute_taxonomy_name($sanitized_title);
        
        if( ! $option['is_require'] ){
            $no_name = __('No');
            $option['values'][] = array(
                'title' => $no_name,
                'price' => 0
            );
            $default_attributes[$sanitized_title] = $no_name;
        }
        foreach ($option['values'] as $vkey => $value) {
            $all[] = "$key/$vkey";
            $available_values[] = $value['title'];
        }
        wp_set_object_terms($wp_product_id, implode('|', $available_values), $attr_taxonomy_name);

        $product_attributes[$sanitized_title] = array(
            'name' => $option['title'],
            'value' => implode('|', $available_values),
            'position' => $option['sort_order'],
            'is_visible' => 1,
            'is_variation' => 1,
            'is_taxonomy' => 0,
            'is_require' => $option['is_require']
        );
        
    }
    $new_meta['_product_attributes'] = $product_attributes;
    if(isset($default_attributes)){
        $new_meta['_default_attributes'] = $default_attributes;
    }

    $combs = mtwi_comb($all, count($mage_custom_options));

    foreach ($combs as $key => $comb) {
        $sum_regular_price = $new_meta['_regular_price'];
        $sum_price = $new_meta['_price'];
        $attr = array();
        $titles = array();
        foreach ($comb as $it) {
            list($i, $j) = explode('/', $it);
            $cur_price = $mage_custom_options[$i]['values'][$j]['price'];
            $cur_title = $mage_custom_options[$i]['title'];
            $sum_regular_price += $cur_price;
            $sum_price += $cur_price;
            $sanitized_title = sanitize_title($cur_title);
            $attr[ wc_variation_attribute_name($sanitized_title) ] = $mage_custom_options[$i]['values'][$j]['title'];
            $titles[] = $cur_title;
        }
        $strtitles = implode(', ', $titles) . "($key)";
        $variation_name = sanitize_title("product-$wp_product_id-$strtitles");
        $variation = array(
            'post_title' => "$strtitles for #$wp_product_id",
            'post_name' => $variation_name,
            'post_status' => 'publish',
            'post_parent' => $wp_product_id,
            'post_type' => 'product_variation',
            'guid' => home_url() . "/?product_variation=$variation_name-$key"
        );
        $var_id = wp_insert_post($variation);
        $var_meta = $attr + array(
            '_regular_price' => $sum_regular_price,
            '_price' => $sum_price,
            '_virtual' => 'no',
            '_downloadable' => 'no',
            '_manage_stock' => 'no',
            '_stock_status' => 'instock'
        );
        if( ! empty($new_meta['_sale_price']) ){
           $var_meta['_sale_price'] = $sum_price;
        }
        foreach ($var_meta as $key_meta => $meta) {
            update_post_meta($var_id, $key_meta, $meta);
        }
    }
    
}

function mtwi_comb(array $strings, $size, array $combinations = array()){
    $filtered = array_map(function($v){ return explode('|', $v);},
                    array_unique(array_map(function($v){ $ex = explode('|', $v); sort($ex); return implode('|', $ex); },
                    _mtwi_comb_all($strings, $size, $combinations))));
    foreach($filtered as $key => $row){
        list($ind) = explode('/', array_shift($row));
        foreach($row as $v){
            list($ind1) = explode('/', $v);
            if($ind1 === $ind){
                unset($filtered[$key]);
                break;
            }
        }
    }
    return array_values($filtered);
}

function _mtwi_comb_all(array $strings, $size, array $combinations = array()) {
    
    if (!$combinations) {
        $combinations = $strings;
    }
    
    if ($size == 1) {
        return $combinations;
    }
    
    $new_combinations = array();
    foreach ($combinations as $combination) {
        foreach ($strings as $char) {
            $new_combinations[] = $combination . '|' . $char;
        }
    }
    return _mtwi_comb_all($strings, $size - 1, $new_combinations);
}

/** @access private */
function _mtwi_process_product_media(array &$new_meta, $product_id, $wp_product_id, $product_title, $created){
    $media_info = mtwi_get_product_media_info($product_id);
    
    if (!$created) {
        $attachments = get_posts(array('post_parent' => $wp_product_id, 'post_type' => 'attachment', 'numberposts' => -1, 'post_status' => null));
        foreach ($attachments as $attach) {
            if (wp_attachment_is_image($attach->ID)) {
                wp_delete_attachment($attach->ID, true);
            }
        }
    }
    $attachments_ids = array();
    foreach ($media_info as $key => $img_info) {
        $attachment_id = mtwi_insert_product_attachment($img_info, $wp_product_id, "$product_title - $key");
        if (!is_wp_error($attachment_id)) {
            $attachments_ids[] = $attachment_id;
        }
    }
    if ($attachments_ids) {
        $new_meta['_thumbnail_id'] = array_shift($attachments_ids);
        $new_meta['_product_image_gallery'] = implode(',', $attachments_ids);
    }
}

/** @access private */
function _mtwi_process_product_stock(array &$new_meta, $product_id){
    $stock_info = mtwi_get_product_stock_info($product_id);
    
    $stock = array_shift($stock_info);
    $new_meta['_manage_stock'] = 'yes';
    if ($stock['is_in_stock'] == 1) {
        $new_meta['_stock_status'] = 'instock';
        $new_meta['_stock'] = intval($stock['qty']);
    } else {
        $new_meta['_stock_status'] = 'outofstock';
    }
}

/** @access private */
function _mtwi_process_product_price(array &$new_meta, $product_price, $product_special_price){
    $new_meta['_price'] = $new_meta['_regular_price'] = floatval($product_price);
    if ($product_special_price) {
        $new_meta['_price'] = $new_meta['_sale_price'] = floatval($product_special_price);
    }
}

/**
 * Get product main info by product id in Magento
 */
function mtwi_get_product_info($product_id) {
    static $products_info = null, $name = 'catalog_product.info';
    return _mtwi_mix_info($products_info, $name, $product_id);
}

/**
 * Get product stock info by product id in Magento
 */
function mtwi_get_product_stock_info($product_id){
    static $products_stock = null, $name = 'cataloginventory_stock_item.list';
    return _mtwi_mix_info($products_stock, $name, $product_id);
}

function mtwi_get_product_linked_info($product_id){
    static $product_linked = null, $name = 'catalog_product_link.list';
    return _mtwi_mix_info($product_linked, $name, $product_id);
}

/**
 * Get product media info by product id in Magento
 */
function mtwi_get_product_media_info($product_id){
    static $products_media = null, $name = 'catalog_product_attribute_media.list';
    return _mtwi_mix_info($products_media, $name, $product_id);
}

function mtwi_get_product_custom_options_info($product_id){
    static $products_options = null, $name = 'product_custom_option.list';
    return _mtwi_mix_info($products_options, $name, $product_id);
}

add_filter('mtwi_soap_data_product_custom_option.list', 'mtwi_get_product_custom_option_filter', 10, 3);
function mtwi_get_product_custom_option_filter($retived_options, SoapClient $client, $session){
    if($retived_options){
        $name = 'product_custom_option.info';
        foreach($retived_options as $option){
            $info = $client->call($session, $name, $option['option_id']);
            $transformed_options[] = $option + array('values' => empty($info['additional_fields']) ? array() : $info['additional_fields']); 
        }
        return $transformed_options;
    }
    return $retived_options;
}

add_filter('mtwi_soap_args_catalog_product_link.list', 'mtwi_product_linked_args');
function mtwi_product_linked_args($args){
    $args[1] = array('type' => 'cross_sell', 'product' => $args[1]);
    return $args;
}

add_filter('mtwi_soap_data_catalog_product_link.list_finally', 'mtwi_get_product_linked_info_filter', 10, 4);
function mtwi_get_product_linked_info_filter($result, SoapClient $client, $session, $args){
    $final_result[$args[1]['type']] = $result;
    $final_result['up_sell'] = $client->call($session, $args[0], array('type' => 'up_sell', 'product' => $args[1]['product']));
    return $final_result;
}

/**
 * Link/add magento categories with the imported/updated WC/WP product
 * 
 * @param int|string $wp_product_id
 * @param array $mage_cats_ids
 * 
 * @return array|WP_Error Result of adding categories
 */
function mtwi_add_cats_to_product($wp_product_id, array $mage_cats_ids){
    $taxonomy = 'product_cat';
    $mage_cats = mtwi_get_categories_list($mage_cats_ids);
    $wp_cats_ids = array();
    
    foreach ($mage_cats as $cat){
        
        $wp_terms_args = array(
            'taxonomy' => $taxonomy,
            'name' => $cat['name'],
            'hide_empty' => 0
        );
        
        if($cat['parent_id']){
            $parent_info = mtwi_get_category_info($cat['parent_id']);
            $wp_parent_cat = get_term_by('slug', $parent_info['url_key'], $taxonomy);
            $wp_terms_args['parent'] = is_object($wp_parent_cat) ? $wp_parent_cat->term_id : 0;
         }
        
        $wp_cats = get_terms($wp_terms_args);
        
        if (isset($wp_cats[0])){
            $wp_cats_ids[] = $wp_cats[0]->term_id;
        }
    }
    
    return wp_set_object_terms( $wp_product_id, $wp_cats_ids, $taxonomy );
}

/**
 * Insert attachment template function
 * 
 * @param string $url
 * @param int|string $wp_parent_id
 * @param array|object|string
 * @param array $mtwi_ajax_error_data
 * 
 * @access private
 * 
 * @return int|WP_Error
 */
function _mtwi_insert_attachment($url, $wp_parent_id = 0, $args = array()){
    global $mtwi_ajax_error_data;
    
    // Need to require these files
    if ( !function_exists('media_handle_upload') ) {
        require_once ABSPATH . "wp-admin" . '/includes/image.php';
        require_once ABSPATH . "wp-admin" . '/includes/media.php';
    }

    $args_parsed = wp_parse_args($args, array(
        'description' => '',
        'title' => '',
        'status' => 'inherit'
    ));
    extract($args_parsed);
    
    $tmp = call_user_func_array('download_url', ini_get('max_input_time') ?
            array($url, empty($mtwi_ajax_error_data['count']) ?
                        ini_get('max_input_time') : 
                        ini_get('max_input_time') / $mtwi_ajax_error_data['count'] ) : 
            array($url));
    if( is_wp_error( $tmp ) ){
        return $tmp;
    }
    $matches = $file_array = array();

    // Set variables for storage
    // fix file filename for query strings
    preg_match('/[^\?]+\.(jpg|jpeg|jpe|gif|png)/i', $url, $matches);
    $file_array['name'] = basename($matches[0]);
    $file_array['tmp_name'] = $tmp;

    // If error storing temporarily, unlink
    if ( is_wp_error( $tmp ) ) {
        @unlink($file_array['tmp_name']);
        $file_array['tmp_name'] = '';
        return $tmp;
    }

    // do the validation and storage stuff
    $attachment_id = media_handle_sideload($file_array, $wp_parent_id, $description, array(
        'post_title' => $title,
        'post_status' => $status
    ));

    // If error storing permanently, unlink
    if ( is_wp_error( $attachment_id ) ) {
        @unlink($file_array['tmp_name']);
        return $attachment_id;
    }

    return $attachment_id;
}

/**
 * Inserting attachment, linking to product
 * 
 * @param array $media_info
 * @param int|string $wp_product_id
 * @param string $product_title
 * 
 * @return int|WP_Error
 */
function mtwi_insert_product_attachment(array $media_info, $wp_product_id, $product_title = ''){
    $attachment_id = _mtwi_insert_attachment($media_info['url'], $wp_product_id, array(
        'title' => $product_title,
        'description' => $media_info['label']
    ));
    return $attachment_id;
}

/**
 * Inserting attachment, seting as thumbnail for the category
 * 
 * @param string $filename
 * @param int|string $wp_term_id
 * @param string $cat_title
 * 
 * @return int|WP_Error
 */
function mtwi_insert_product_cat_attachment($filename, $wp_term_id, $cat_title){
    if( !function_exists('update_woocommerce_term_meta') ){
        return;
    }
    
    $attachment_id = _mtwi_insert_attachment(mtwi_get_base_url() . "/media/catalog/category/$filename", 0, array( 'title' => $cat_title ));
    if( is_wp_error($attachment_id) ){
        return $attachment_id;
    }
    
    update_woocommerce_term_meta( $wp_term_id, 'thumbnail_id', intval($attachment_id) );
    
    return $attachment_id;
}

/**
 * Search recursively an array with pair <b>$nkey</b> => <b>$nvalue</b>
 * 
 * @param array $array 
 * @param mixed $nkey
 * @param mixed $nvalue
 * 
 * @return array|false Found element, <b>false</b> if not found
 */
function mtwi_recursive_search(array $array, $nkey, $nvalue){
    $iterator  = new RecursiveArrayIterator($array);
    $recursive = new RecursiveIteratorIterator(
        $iterator,
        RecursiveIteratorIterator::SELF_FIRST
    );
    foreach ($recursive as $value) {
        if ( is_array($value) && isset($value[$nkey]) && $value[$nkey] == $nvalue ) {
            return $value;
        }
    }
    return false;
}

/**
 * Getting the base url of Magento
 * @return string|false Base url
 */
function mtwi_get_base_url(){
    global $mtwi_options;
    $res = strstr($mtwi_options['mage_soap_url'], 'index.php', true);
    if( $res ){
        $url = $res;
    } else {
        $url = strstr($mtwi_options['mage_soap_url'], 'api', true);
    }
    return untrailingslashit($url);
}

/**
 * @sub-package MTWI_Debug
 * 
 * @param bool $die Die after and output results?
 * @param mixed $arg... Args to be outputed or saved if <b>$die</b> is <i>false</i>
 */
function mtwi_dump() {
    static $content = null;
    if (ob_get_length()) {
        ob_end_clean();
    }
    $args = func_get_args();
    $die = array_shift($args);
    foreach ($args as $arg) {
        print('<pre>');
        print_r($arg);
        print('</pre>');
    }
    $content .= ob_get_clean();
    if ( $die ) {
        wp_die($content);
    }
}

/**
 * @sub-package MTWI_Debug
 * 
 * @param int|string $product_id ID if Magento product to be imported
 * @return bool
 */
function mtwi_import_product($product_id){
    $data = mtwi_get_products_list(array($product_id));
    return mtwi_import_products($data);
}

/**
 * @sub-package MTWI_Debug
 * 
 * @param int|string $customer_id ID if Magento customer to be imported
 * @return bool
 */
function mtwi_import_customer($customer_id){
    $data = mtwi_get_customers_list(array($customer_id));
    return mtwi_import_customers($data);
}

/**
 * @sub-package MTWI_Debug
 * 
 * @param int|string $category_id ID if Magento category to be imported
 * @return bool
 */
function mtwi_import_category($category_id){
    $data = mtwi_get_categories_list(array($category_id));
    return mtwi_import_categories($data);
}

/**
 * @param array $orders
 * @global wpdb $wpdb
 * @global int $user_ID
 * @global bool $mtwi_is_last_request
 */
function mtwi_import_orders(array $orders){
    global $wpdb, $user_ID, $mtwi_is_last_request;
    
    foreach($orders as $order_data) {
        $increment_id = $order_data['increment_id'];
        $order_details = array();
        if($increment_id){
            $order_details = mtwi_get_order_info($increment_id);
        }
        $mage_customer_email = $order_details['customer_email'];
        $wp_customer_id = email_exists($mage_customer_email);

        $order_id = 0;
        if(!empty($order_details) && isset($order_details['increment_id'])) {
            $order_data['post_date'] = $order_details['created_at'];
            $order_data['post_author'] = $user_ID;
            $order_data['post_type'] = 'shop_order';
            $order_data['post_excerpt'] = '';
            $order_status = $order_details['status'];
            $wp_order_status = mtwi_get_wp_order_status($order_status);
            $order_data['post_status'] = $wp_order_status;
            $str = "SELECT post_id FROM $wpdb->postmeta, $wpdb->posts WHERE meta_key = '_transaction_id'"
                    . " && post_id = ID && post_type = 'shop_order' && meta_value = '{$order_details['payment']['payment_id']}'";
            $post_id = intval( $wpdb->get_var($str) );
            if($post_id){
                $order_id = $post_id;
                wp_update_post($order_data);
                $wc_order = wc_get_order($order_id);
                $items = $wc_order->get_items();
                while(list($item_id) = each($items))
                    wc_delete_order_item($item_id);
            } else {
                $order_id = wp_insert_post($order_data); 
            }
        }
        
        $order_metadata['_payment_method_title'] = $order_details['payment']['cc_type'];
        $order_metadata['_payment_method'] = $order_details['payment']['method'];
        $order_metadata['_transaction_id'] = $order_details['payment']['payment_id'];
        $order_metadata['_billing_first_name'] = $order_details['billing_address']['firstname'];
        $order_metadata['_billing_last_name'] = $order_details['billing_address']['lastname'];
        $order_metadata['_billing_company'] = $order_details['billing_address']['company'];
        $order_metadata['_billing_address_1'] = $order_details['billing_address']['street'];
        //$order_metadata['_billing_address_2'] = $orderDetails['payment'][''];
        $order_metadata['_billing_city'] = $order_details['billing_address']['city'];
        $order_metadata['_billing_postcode'] = $order_details['billing_address']['postcode'];
        $order_metadata['_billing_state'] = $order_details['billing_address']['region'];
        $order_metadata['_billing_country'] = $order_details['billing_address']['country_id'];
        $order_metadata['_billing_phone'] = $order_details['billing_address']['telephone'];
        $order_metadata['_billing_email'] = $order_details['billing_address']['email'];
        $order_metadata['_shipping_first_name'] = $order_details['shipping_address']['firstname'];
        $order_metadata['_shipping_last_name'] = $order_details['shipping_address']['lastname'];
        $order_metadata['_shipping_company'] = $order_details['shipping_address']['company'];
        $order_metadata['_shipping_address_1'] = $order_details['shipping_address']['street'];
        //$order_metadata['_shipping_address_2'] = $orderDetails['payment'][''];
        $order_metadata['_shipping_city'] = $order_details['shipping_address']['city'];
        $order_metadata['_shipping_postcode'] = $order_details['shipping_address']['postcode'];
        $order_metadata['_shipping_state'] = $order_details['shipping_address']['region'];
        $order_metadata['_shipping_country'] = $order_details['shipping_address']['country_id'];
        if($wp_customer_id)
            $order_metadata['_customer_user'] = $wp_customer_id;
        $order_metadata['_order_currency'] = $order_details['order_currency_code'];
        $order_metadata['_order_shipping_tax'] = $order_details['base_shipping_tax_amount'];
        $order_metadata['_order_tax'] = $order_details['tax_amount'];
        $order_metadata['_order_total'] = $order_details['payment']['amount_ordered'];
        $order_metadata['_cart_discount_tax'] = 0;
        $order_metadata['_cart_discount'] = 0;
        $order_metadata['_order_shipping'] = $order_details['payment']['shipping_amount'];
        $order_metadata['_refund_amount'] = $order_details['payment']['amount_refunded']; 

        if($order_id) {

            foreach($order_metadata as $meta_key => $meta_value) {
                update_post_meta($order_id, $meta_key, $meta_value);
            }

            // Order - Items Details
            $ordered_items = $order_details['items'];

            foreach($ordered_items as $item_data) {
                $item_metadata['order_item_name'] = $item_data['name'];
                $item_metadata['order_item_type'] = 'line_item';
                $item_metadata['_line_subtotal'] = $order_details['base_subtotal'];
                $item_metadata['_line_subtotal_tax'] = 0;
                $item_metadata['_line_total'] = $order_details['base_grand_total'];
                $item_metadata['_line_tax'] = $item_data['tax_amount'];
                $item_metadata['_line_tax_data'] = 'a:2:{s:5:"total";a:0:{}s:8:"subtotal";a:0:{}} | a:2:{s:5:"total";a:0:{}s:8:"subtotal";a:0:{}}';
                $item_metadata['_tax_class'] = 'reduced-rate';
                list($item_metadata['_qty']) = explode('.', $item_data['qty_ordered']);

                $item = array(
                    'order_item_name' => $item_data['name'],
                    'order_item_type' => 'line_item'
                );
                $item_id = wc_add_order_item($order_id, $item);
                
                $wp_product_id = wc_get_product_id_by_sku($item_data['sku']);
                if($wp_product_id)
                    wc_add_order_item_meta($item_id, '_product_id', $wp_product_id);
                
                if($item_metadata['_variation_id'])
                        wc_add_order_item_meta( $item_id, '_variation_id', $item_metadata['_variation_id']);
                wc_add_order_item_meta( $item_id, '_line_subtotal', $item_metadata['_line_subtotal']);
                wc_add_order_item_meta( $item_id, '_line_subtotal_tax', $item_metadata['_line_subtotal_tax']);
                wc_add_order_item_meta( $item_id, '_line_total', $item_metadata['_line_total']);
                wc_add_order_item_meta( $item_id, '_line_tax', $item_metadata['_line_tax']);
                if($item_metadata['_line_tax_data']) {
                    $unserialized_tax_data = unserialize($item_metadata['_line_tax_data']);
                    wc_add_order_item_meta( $item_id, '_line_tax_data', $unserialized_tax_data);
                }
                wc_add_order_item_meta( $item_id, '_tax_class', $item_metadata['_tax_class']);
                wc_add_order_item_meta( $item_id, '_qty', $item_metadata['_qty']);
            }

            // Order - Shipment Details
            $shipment_metadata['order_item_name'] = $order_details['shipping_description'];
            $shipment_metadata['method_id'] = 'flat_rate';
            $shipment_metadata['cost'] = $order_details['shipping_amount'];
            $shipment_metadata['taxes'] = '';
            $item = array(
                'order_item_name'     => $shipment_metadata['order_item_name'],
                'order_item_type'     => 'shipping',
            );
            $item_id = wc_add_order_item($order_id, $item);

            wc_add_order_item_meta( $item_id, 'method_id', $shipment_metadata['method_id']);
            wc_add_order_item_meta( $item_id, 'cost', $shipment_metadata['cost']);
            if($shipment_metadata['taxes']) {
                $unserialized_tax_data = unserialize($shipment_metadata['taxes']);
                wc_add_order_item_meta( $item_id, 'taxes', $unserialized_tax_data);
            }
        }
    }
    
    if($mtwi_is_last_request){
        // Calculate totals for products based on orders
        $wp_products_total = $wpdb->get_results("SELECT order_item_meta_2.meta_value as product_id, SUM( order_item_meta.meta_value ) as line_total
                FROM {$wpdb->prefix}woocommerce_order_items as order_items
                LEFT JOIN {$wpdb->prefix}woocommerce_order_itemmeta as order_item_meta ON order_items.order_item_id = order_item_meta.order_item_id
                LEFT JOIN {$wpdb->prefix}woocommerce_order_itemmeta as order_item_meta_2 ON order_items.order_item_id = order_item_meta_2.order_item_id
                LEFT JOIN $wpdb->posts AS posts ON order_items.order_id = posts.ID
                WHERE posts.post_type = 'shop_order' 
                AND posts.post_status = 'wc-completed' 
                AND order_items.order_item_type = 'line_item' 
                AND order_item_meta.meta_key = '_qty' 
                AND order_item_meta_2.meta_key = '_product_id' 
                GROUP BY order_item_meta_2.meta_value"
        );
        foreach($wp_products_total as $wp_product_total){
            update_post_meta($wp_product_total->product_id, '_recorded_sales', 'yes');
            update_post_meta($wp_product_total->product_id, 'total_sales', $wp_product_total->line_total);
        }
    }
    
    return true;
}

function mtwi_get_order_info($order_id){
    static $addresses = null, $name = 'sales_order.info';
    return _mtwi_mix_info($addresses, $name, $order_id);
}

function mtwi_get_wp_order_status($mage_order_status) {
    switch($mage_order_status){
        case 'canceled': 
            return 'wc-cancelled';
        case 'closed':
            return 'wc-failed';
        case 'complete':
            return 'wc-completed';
        case 'fraud':
            return 'wc-failed';
        case 'holded':
            return 'wc-on-hold';
        case 'payment_review':
            return 'wc-processing';
        case 'pending': 
            return 'wc-pending';
        case 'pending_payment':
            return 'wc-pending';
        case 'pending_paypal':
            return 'wc-pending';
        case 'processing':
            return 'wc-processing';
        default: 
            return '';
    }
}


function mtwi_delete_all_products() {
    $post_type = 'product';
    $wp_products = get_posts(array(
        'post_type' => $post_type, 
        'post_status' => array('publish', 'draft'),
        'posts_per_page' => -1 
    ));
    
    if($wp_products){
        foreach($wp_products as $product){
            $galery = explode(',', get_post_meta($product->ID, '_product_image_gallery', true));
            foreach($galery as $image){
                wp_delete_attachment(intval( $image ), true);
            }
            wp_delete_attachment( intval( get_post_meta($product->ID, '_thumbnail_id', true) ), true);
            wp_delete_post($product->ID, true);
        }
    }
}